import './Product.css';

const Product = ({product}) => {
  return <div className="product">
    <p className="product-header">Product Name: {product.name}</p>
    <p className="product-header">Product Price: {product.price}</p>
    {product.quantity > 0 
      ? <p className="product-header">Quantity: {product.quantity}</p> 
      : <p className="product-header error">Sorry! This product is out of stock.</p>
    }
  </div>
}

export default Product;