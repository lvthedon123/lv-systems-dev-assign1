import {useEffect, useState} from 'react';
import {parseStringPromise} from 'xml2js';
import Product from './Product';
import productsJson from './products.json';
import './App.css';

function App() {
  const [dataSourceType, setDataSourceType] = useState('json');
  const [parsedXml, setParsedXml] = useState();
  const [jsonData, setJsonData] = useState(productsJson);

  useEffect(() => {
    fetch('./productsxml.xml')
      .then((response) => response.text())
      .then((responseText) => {
        parseStringPromise(responseText)
          .then((result) => {
            setParsedXml(result);
          })
      })
  }, []);

  const handleDataSourceSwitchClick = () => {
    setDataSourceType((prevState) => {
      return prevState === 'json' ? 'xml' : 'json';
    });
  }

  const renderJson = () => {
    return jsonData.products.map((product) => {
      return <Product product={product} />
    })
  }

  const renderXml = () => {
    if (!parsedXml.root) {
      return null;
    }

    return parsedXml.root.products[0].product.map((product) => {
      return <Product product={{name: product.name[0], price: product.price[0], quantity: product.quantity[0]}} />
    })
  }

  return (
    <div className="App">
      <h1>XML and JSON Viewer: Created by Leonard Vuniqi</h1>
      <h3>Current data source: {dataSourceType}</h3>
      <button onClick={handleDataSourceSwitchClick}>Switch the data source to {dataSourceType === 'json' ? 'xml' : 'json'}</button>
      <section className="product-container">
        {dataSourceType === 'json' ? renderJson() : renderXml()}
      </section>
    </div>
  );
}

export default App;
