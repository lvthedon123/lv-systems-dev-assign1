# Clone

Clone this project

## Install dependencies

In your terminal run `npm install` and let it install all the dependencies

### Start the application with `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.
